<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>掲示板SYSTEM</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

        	<div class="header1">
        	<h1>社員情報PAGE</h1>
        	</div>

            <div class="header2">
                <a href="signup">社員新規登録</a>
            </div>

			  <div class="users">
			    	<c:forEach items="${userinfo}" var="users">
			    		<div class="users">
			    			<div class ="id">
			        			<span class="users">
			       	 			<c:out value="${users.id}" />
			       	 			</span>

			        			<span class="name">
			        			<c:out value="${users.name}" />
			        			</span>

			        			<span class="branches_name">
			            		<c:out value="${users.branches_name}" />
			            		</span>

			        			<span class="positions_name">
			            		<c:out value="${users.positions_name}" />
			            		</span>

			            		<form action = "settings" method = "get">
			            		<input name = "Id" value ="${users.id}"type="hidden" />

			            		<input type="submit" value="編集" />
			            		</form>

			       		 	</div>
			       		</div>
			    	</c:forEach>
				</div>
            <div class="copyright"> Copyright(c)yasuda_rina</div>
        </div>
    </body>
</html>
