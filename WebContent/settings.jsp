<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>社員情報更新</title>
        <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

        	<div class="header1">
        		<h1>社員情報PAGE</h1>
        	</div>

            <c:if test="${ not empty errorMessages }">
               	<div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session"/>
            </c:if>

			<div class="form">

            	<form action="settings" method="post"><br />

                	<input name="Id" value="${edit.id}" id="id" type="hidden"/>

                	<label for="login_id">ログインID</label>
                	<input name="login_id" value="${edit.login_id}" id="login_id"><br />

                	<label for="name">名前</label>
                	<input name="name" value="${edit.name}" id="name"/> <br />

                	<label for="password">パスワード</label>
                	<input name="password" type="password" id="password"/>

                	<label for="password2">確認用パスワード</label>
                	<input name="password2" type="password" id="password2"/> <br />

                	<label for="branch">支店</label>
					<select name="branch" size="1" id="branch" >
               			<c:forEach items="${branch}" var="bra">
		                	<c:if test="${bra.id == edit.branch}">
						 		<option value="${bra.id}" selected>${bra.branches_name}</option>
							</c:if>
							<c:if test="${bra.id != edit.branch}">
							 	<option value="${bra.id}"><c:out value="${bra.branches_name}"/></option>
							</c:if>
                    	</c:forEach>
                	</select><br />

                	<label for="position">部署・役職</label>
                	<select name="position" size="1" id="position">
                		<c:forEach items="${position}" var="po">
                			<c:if test="${po.id == edit.position}">
						 		<option value="${po.id}" selected>${po.positions_name}</option>
							</c:if>
							<c:if test="${po.id != edit.position}">
						 		<option value="${po.id}"><c:out value="${po.positions_name}"/></option>
						</c:if>
                    	</c:forEach>
                	</select><br />

                	<input type="submit" value="更新" /> <br />

                	<a href="./">戻る</a>

            	</form>
            	<div class="copyright"> Copyright(c)yasuda_rina</div>
            </div>
        </div>
    </body>
</html>