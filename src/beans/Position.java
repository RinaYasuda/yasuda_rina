package beans;

import java.io.Serializable;

public class Position implements Serializable {
    private static final long serialVersionUID = 1L;

    private int id;
    private String positions_name;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getPositions_name() {
		return positions_name;
	}
	public void setPositions_name(String positions_name) {
		this.positions_name = positions_name;
	}
}
