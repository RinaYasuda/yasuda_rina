package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import beans.UserInfo;
import service.BranchService;
import service.PositionService;
import service.UserInfoService;
import service.UserService;

@WebServlet(urlPatterns = { "/settings" })
public class SettingServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,HttpServletResponse response)
    		throws ServletException, IOException {

        int value = Integer.parseInt(request.getParameter("Id"));
        UserInfo edit = new UserInfoService().getEdit(value);
        request.setAttribute("edit", edit);

        List<Branch> branch = new BranchService().getBranch();
    	request.setAttribute("branch", branch);

    	List<Position> position = new PositionService().getPosition();
    	request.setAttribute("position", position);

        request.getRequestDispatcher("settings.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws ServletException, IOException {

        List<String> messages = new ArrayList<String>();
        List<Branch> branch = new BranchService().getBranch();
        List<Position> position = new PositionService().getPosition();
        HttpSession session = request.getSession();
        User editUser = getEditUser(request);

        if (isValid(request, messages) == true) {

            new UserService().update(editUser);
            response.sendRedirect("./");

        } else {
            session.setAttribute("errorMessages", messages);
            request.setAttribute("editUser", editUser);
            request.setAttribute("branch", branch);
        	request.setAttribute("position", position);
            request.getRequestDispatcher("settings.jsp").forward(request, response);
        }
    }

    private User getEditUser(HttpServletRequest request)
            throws IOException, ServletException {

        User editUser = new User();
        editUser.setId(Integer.parseInt(request.getParameter("Id")));
        editUser.setLogin_id(request.getParameter("login_id"));
        editUser.setName(request.getParameter("name"));
        editUser.setPassword(request.getParameter("password"));
        editUser.setPassword(request.getParameter("password2"));
        editUser.setBranch(Integer.parseInt(request.getParameter("branch")));
        editUser.setPosition(Integer.parseInt(request.getParameter("position")));

        return editUser;
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");

        UserInfo edit = new UserInfoService().getLogin_id(login_id);

        if(!login_id.matches("^[a-zA-Z0-9]{6,20}$")) {
        	messages.add("ログインIDは半角英数字で6文字以上20文字以下です");
        }

        if(!(edit != null)) {
        	messages.add("そのアカウントは存在しています");
        }

        if (StringUtils.isEmpty(password) == true) {
        } else if(!password.matches("^[a-zA-Z0-9 -~]{6,20}$")) {
        	messages.add("パスワードは半角文字で6文字以上20文字以下です");
        } else if(!password2.equals(password)) {
        	messages.add("パスワードが一致していません");
        }

        if(name.length() > 10) {
        	messages.add("ユーザー名は10文字以内で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}

