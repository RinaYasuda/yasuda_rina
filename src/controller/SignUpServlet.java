package controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.apache.commons.lang.StringUtils;

import beans.Branch;
import beans.Position;
import beans.User;
import service.BranchService;
import service.PositionService;
import service.UserService;

@WebServlet(urlPatterns = { "/signup" })
public class SignUpServlet extends HttpServlet {
    private static final long serialVersionUID = 1L;

    @Override
    protected void doGet(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

    	List<Branch> branch = new BranchService().getBranch();
    	request.setAttribute("branch", branch);

    	List<Position> position = new PositionService().getPosition();
    	request.setAttribute("position", position);

        request.getRequestDispatcher("signup.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request,
            HttpServletResponse response) throws IOException, ServletException {

        List<String> messages = new ArrayList<String>();

        HttpSession session = request.getSession();
        if (isValid(request, messages) == true) {

            User user = new User();
            user.setLogin_id(request.getParameter("login_id"));
            user.setPassword(request.getParameter("password"));
            user.setPassword2(request.getParameter("password2"));
            user.setName(request.getParameter("name"));
            user.setBranch(Integer.parseInt(request.getParameter("branch")));
        	user.setPosition(Integer.parseInt(request.getParameter("position")));

            new UserService().register(user);

            response.sendRedirect("./");
        } else {
            session.setAttribute("errorMessages", messages);
            response.sendRedirect("signup");
        }
    }

    private boolean isValid(HttpServletRequest request, List<String> messages) {
        String login_id = request.getParameter("login_id");
        String password = request.getParameter("password");
        String password2 = request.getParameter("password2");
        String name = request.getParameter("name");

        if (StringUtils.isEmpty(login_id) == true) {
            messages.add("ログインIDを入力してください");
        }else if(!login_id.matches("^[a-zA-Z0-9]{6,20}$")) {
        	messages.add("ログインIDは半角英数字で6文字以上20文字以下です");
        }

        if (StringUtils.isEmpty(password) == true) {
            messages.add("パスワードを入力してください");
        }else if(!password.matches("^[a-zA-Z0-9 -~]{6,20}$")) {
        	messages.add("パスワードは半角文字で6文字以上20文字以下です");
        } else if(!password2.equals(password)) {
        	messages.add("パスワードが一致していません");
        }

        if(name.length() > 10) {
        	messages.add("名前は10文字以内で入力してください");
        }

        if (messages.size() == 0) {
            return true;
        } else {
            return false;
        }
    }
}