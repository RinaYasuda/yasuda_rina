<%@page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@page isELIgnored="false"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
    <head>
    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
    <title>社員新規登録</title>
    <link href="./css/style.css" rel="stylesheet" type="text/css">
    </head>
    <body>
        <div class="main-contents">

        	<div class="header1">
        		<h1>社員新規登録画面</h1>
        	</div>

            <c:if test="${ not empty errorMessages }">
                <div class="errorMessages">
                    <ul>
                        <c:forEach items="${errorMessages}" var="message">
                            <li><c:out value="${message}" />
                        </c:forEach>
                    </ul>
                </div>
                <c:remove var="errorMessages" scope="session" />
            </c:if>

            <div class="form">

            	<form action="signup" method="post"><br />
                	<label for="login_id">ログインID</label>
                	<input name="login_id" id="login_id" /><br />

                	<label for="password">パスワード</label>
               	 	<input name="password" type="password" id="password" /> <br />

                	<label for="password2">パスワード確認用</label>
                	<input name="password2" type="password" id="password2" /> <br />

                	<label for="name">社員名</label>
                	<input name="name" id="name" /> <br />

                	<label for="branch">支店</label>
                	<select name="branch" size=1 id="branch">
               			<c:forEach items="${branch}" var="bra">
	                		<option value="${bra.id}"><c:out value="${bra.branches_name}"/></option>
                    	</c:forEach>
                	 </select><br />

                	<label for="position">部署・役職</label>
                	 <select name="position" size=1 id="position">
                		<c:forEach items="${position}" var="po">
	                		<option value="${po.id}"><c:out value="${po.positions_name}"/></option>
                    	</c:forEach>
                	 </select><br />

                	<input type="submit" value="登録" /> <br />

                	<a href="./">戻る</a>

            	</form>
            	<div class="copyright">Copyright(c)yasuda_rina </div>
            </div>
        </div>
    </body>
</html>